[[!toc  startlevel=2]]

## About Backports ##

Debian Backports provides new packages with new features on supported
Debian stable releases.

As a matter of Backports policy, packages in the stable-backports
suite are taken from Debian testing; however, in rare cases such as
security updates, packages are taken from unstable. Backports Policy
permits this exclusively when such packages will be part of the next
Debian stable release. In any case, the new packages are recompiled
for use on Debian stable. Backports are expected to make use of the
versions of libraries available in Debian stable whenever possible;
however, when required, the backporting of additional dependencies is
permitted.

When a new Debian release is made, the previous stable suite becomes
old-stable. While old stable releases may have [Long Term
Support](https://wiki.debian.org/LTS), old-stable-backports are only
made available for a period of one year after a new Debian stable
release has been made. For example, Debian Buster was released 6 July
2019 and stretch-backports was retired one year later. Packages in
old-stable-backports must track the versions in stable (or
stable/updates for security fixes).

Backports cannot be tested as extensively as Debian stable, and are
thus supported on a best-effort basis; there is a risk of
incompatibilities with other components in Debian stable, so backports
should be used with care! That said, Debian Backports Policy does not
allow backports of libraries that would break all dependent packages
in stable (eg: new Qt 5.x releases), and by virtue of this, Debian
Backports are considered generally safe when used as intended on an
individual package basis.

The coinstallability of all available backports is not tested, and it
is strongly recommended to opt-into the use of specific backported
packages on an as-needed basis.


## Add Backports to sources.list

1.  <pre>deb http://deb.debian.org/debian bookworm-backports main</pre> to **sources.list** 
    (or add a new file with the ".list" extension to /etc/apt/sources.list.d/).

2. Run **apt update** or **apt-get update**

## Installing a Package from Backports

All backports are deactivated by default so that the normal operation
of a stable installation will not be compromised with potentially
disruptive changes (such as incompatible configuration schema).
Release files make this possible, and all backported packages are
pinned to priority 100 via `ButAutomaticUpgrades: yes`. To install
something from backports run one of:

    apt install <package>/bookworm-backports
    apt-get install <package>/bookworm-backports

or

    apt install -t bookworm-backports <package>
    apt-get install -t bookworm-backports <package>

and of course aptitude may also be used:

    aptitude install <package>/bookworm-backports


## The Old-stable-sloppy Suite

To guarantee a clean upgrade path from one Debian stable release to
the next, packages in Backports cannot be newer than the packages
destined for the next Debian stable release; this is a matter of
Backports Policy. Anyone who is willing to sacrifice the ability to
cleanly and smoothly upgrade between stable releases may "get the
latest version available" via old-stable-sloppy. Old-stable-sloppy
backports are packages from testing that have been rebuilt for
old-stable.

Enabling old-stable-sloppy-backports is near identical to
stable-backports, with two notable exceptions.  First, in
**sources.list**. Rather than using:

    deb http://deb.debian.org/debian bullseye-backports main

use this instead:

    deb http://deb.debian.org/debian bullseye-backports-sloppy main

Second, Packages in backports-sloppy are provided "as-is" and have no
official support. Like old-stable-backports,
old-stable-backports-sloppy is taken offline one year after a new
Debian release has been made.


## Subscribe to the Security Announcements Mailing List (highly recommended)

To receive announcements about security updates for Debian Backports, subscribe to the [debian-backports-announce mailing list](https://lists.debian.org/debian-backports-announce/). 

## Report Bugs

Please report bugs in backported packages to the [backports mailing list](https://lists.debian.org/debian-backports/) and **NOT** to the Debian BTS! 
